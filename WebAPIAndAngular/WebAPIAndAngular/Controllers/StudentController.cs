﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebAPIAndAngular.Models;

namespace WebAPIAndAngular.Controllers
{
    public class StudentController : ApiController
    {
        private List<StudentViewModel> _studentList;
        public StudentController()
        {
            PrepareData();
        }
      

        private void PrepareData()
        {
            const string sessionListName = "StudentList";

            if (HttpContext.Current.Session[sessionListName] == null)
            {
                _studentList = new List<StudentViewModel>()
                {
                    new StudentViewModel()
                    {
                        Id = 1,
                        FirstName = "Robert",
                        LastName = "Lewandowski",
                        Rate = 3,
                        University = "Politechnika Śląska"
                    },
                    new StudentViewModel()
                    {
                        Id = 2,
                        FirstName = "Turbo",
                        LastName = "Grosik",
                        Rate = 5,
                        University = "Uniwersytet Śląski"
                    },
                    new StudentViewModel()
                    {
                        Id = 3,
                        FirstName = "Jakub",
                        LastName = "Błaszczykowski",
                        Rate = 4,
                        University = "AGH"
                    }
                };

                HttpContext.Current.Session[sessionListName] = _studentList;
            }
            else
            {
                _studentList = (List<StudentViewModel>)HttpContext.Current.Session["StudentList"];
            }
        }
    }
}
