﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIAndAngular.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
               
        public string FirstName { get; set; }
      
        public string LastName { get; set; }
      
        public int Rate { get; set; }
     
        public string University { get; set; }
    }
}